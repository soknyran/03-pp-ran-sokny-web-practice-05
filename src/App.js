import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import MyCard from "./components/MyCard";
import { Container, Row } from "react-bootstrap";
import NavMenu from "./components/NavMenu";
import MyTable from "./components/MyTable";
import Content from "./components/Content";

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            drinks: [
                {
                    id: 1,
                    title: "Coffee Frappe",
                    img: "image/coffee_frappe.jpg",
                    amount: 0,
                    price: 5,
                    total: 0,
                },
                {
                    id: 2,
                    title: "Luna Cold Coffee",
                    img: "image/luna_cold_coffee.jpg",
                    amount: 0,
                    price: 3,
                    total: 0,
                },
                {
                    id: 3,
                    title: "Strawberry Soy Milk",
                    img: "image/strawberry_soy_milk.jpg",
                    amount: 0,
                    price: 5,
                    total: 0,
                },
                // {
                //     id: 4,
                //     title: "The Love",
                //     img: "image/the_love.jpg",
                //     amount: 0,
                //     price: 3,
                //     total: 0,
                // },
                // {
                //     id: 5,
                //     title: "Iced Galaxy",
                //     img: "image/iced_galaxy.jpg",
                //     amount: 0,
                //     price: 5,
                //     total: 0,
                // },
                // {
                //     id: 6,
                //     title: "Caramel Frappe",
                //     img: "image/caramel_frappe.jpg",
                //     amount: 0,
                //     price: 3,
                //     total: 0,
                // },
            ],
        };
    }

    onAdd = (index) => {
        console.log("index:", index);
        let temp = [...this.state.drinks]
        temp[index].amount++
        temp[index].total = temp[index].amount * temp[index].price
        this.setState({
            drinks: temp
        })
    }

    onDelete = (index) => {
        console.log("index:", index);
        let temp = [...this.state.drinks]
        temp[index].amount--
        temp[index].total = temp[index].amount * temp[index].price
        this.setState({
            drinks: temp
        })
    }

    onClear = () => {
        console.log("Clearing...");
        let temp = [...this.state.drinks]
        temp.map(item => {
            item.amount = 0
            item.total = 0
        })
        this.setState({
            drinks: temp
        })
    }

    render() {
        return (
            <div>
                <NavMenu />
                <Content/>
                <Container>
                    <Row>
                        <MyCard
                            drinks={this.state.drinks}
                            onAdd={this.onAdd}
                            onDelete={this.onDelete}
                        />
                    </Row>
                    <Row>
                        <MyTable
                            drinks={this.state.drinks}
                            onClear={this.onClear}
                        />
                    </Row>
                </Container>
            </div>
        );
    }
}
