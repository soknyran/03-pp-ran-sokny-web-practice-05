import React, { Component } from 'react'
import { Card, Button, Col } from 'react-bootstrap'

class MyCard extends Component {



    render() {
        console.log("Drinks Props:", this.props);

        return (
            <>{
                this.props.drinks.map((item, index) => (
                    <Col xs="4" key={index}>
                        <Card>
                            <Card.Img variant="top" src={item.img} />
                            <Card.Body>
                                <Card.Title><p>{item.title}</p></Card.Title>
                                <Card.Text>
                                    Price : {item.price} $
                                </Card.Text>
                                <Button disabled={true} size="xl" variant="warning">{item.amount}</Button>
                                <Button className="mx-2" onClick={()=>this.props.onAdd(index)} variant="primary">Add</Button>
                                <Button disabled={item.amount === 0?true: false} onClick={()=>this.props.onDelete(index)} variant="danger">Delete</Button>
                                <h5 className="my-2">Total : {item.total} $</h5>
                            </Card.Body>
                        </Card>
                    </Col>
                )
                )
            }</>
        )
    }
}

export default MyCard;