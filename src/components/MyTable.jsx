import React from 'react'
import { Table, Button } from 'react-bootstrap'

export default function MyTable({ drinks, onClear }) {
    console.log("Props on Function:", drinks);

    let temp = drinks.filter(item => {
        return item.amount > 0
    })

    return (
        <>
            <Button className="my-2" onClick={onClear} variant="warning">Reset All</Button>
            <Button className="mx-2 my-2" disabled={true} size="xl" variant="warning">{temp.length} Drink</Button>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Drink</th>
                        <th>Amount</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    {temp.map((item, index) => (
                        <tr>
                            <td>{index + 1}</td>
                            <td>{item.title}</td>
                            <td>{item.amount}</td>
                            <td>{item.price}</td>
                            <td>{item.total}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </>
    )
}
