import React from 'react'
import { Container, Navbar } from 'react-bootstrap'

function NavMenu() {
    return (
        <Navbar>
            <Container>
                <Navbar.Brand href="#home"><p>Menu</p></Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        Design By : <a href="#login">Sokny Ran</a>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default NavMenu;